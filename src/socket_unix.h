#ifndef SOCKET_UNIX_H
#define SOCKET_UNIX_H

int socket_bind_unix(const char *socket_name, int *fd);
int socket_connect_unix(const char *socket_name, int *fd);

#endif
