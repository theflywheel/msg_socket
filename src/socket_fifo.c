#include <string.h>
#include <pthread.h>
#include <sys/eventfd.h>

#include "socket_common.h"
#include "socket_defs.h"
#include "socket_fifo.h"

#define MAX_SOCKET_NAME_LEN 256
#define FIFO_BUFFER_LEN 100

struct socket_list_s {
    struct fifo_descriptor_s fifo;
    char name[MAX_SOCKET_NAME_LEN];
    struct socket_list_s *next;
};

static struct socket_list_s *head = NULL;
static pthread_mutex_t fifo_list_mutex = PTHREAD_MUTEX_INITIALIZER;

// This function should be called inside mutex block
static struct socket_list_s* fifo_find(const char* name) 
{
    struct socket_list_s *current_element = head;
    while( current_element ) {
        if( 0 == strncmp( current_element->name, name, MAX_SOCKET_NAME_LEN) ) {
            return current_element;
        }
    }
    return NULL;
}

int socket_bind_fifo(const char* socket_name, struct fifo_descriptor_s *fifo_descriptor) 
{
    struct socket_list_s *new_fifo;
    int result = 0;

    fifo_descriptor->fifo_buffer_in = NULL;
    fifo_descriptor->fifo_buffer_out = NULL;
    fifo_descriptor->event_fd_in = -1;
    fifo_descriptor->event_fd_out = -1;

    pthread_mutex_lock(&fifo_list_mutex);

    if( NULL != fifo_find(socket_name) ) {
        last_error = MSG_SOCKET_FIFO_ALREADY_EXISTS;
        result = -1;
        goto finish;
    }

    new_fifo = (struct socket_list_s*)calloc(1, sizeof(struct socket_list_s));
    if( NULL == new_fifo ) {
        last_error = MSG_SOCKET_OUT_OF_MEMORY;
        result = -1;
        goto finish;
    }
    strncpy(new_fifo->name, socket_name, MAX_SOCKET_NAME_LEN);
    fifo_create(&new_fifo->fifo.fifo_buffer_in, FIFO_BUFFER_LEN);  
    fifo_create(&new_fifo->fifo.fifo_buffer_out, FIFO_BUFFER_LEN);  
    new_fifo->fifo.event_fd_in = eventfd(0, EFD_SEMAPHORE);
    new_fifo->fifo.event_fd_out = eventfd(0, EFD_SEMAPHORE);
    
    *fifo_descriptor = new_fifo->fifo;

    if(NULL == head) {
        head = new_fifo;
    } else {
        new_fifo->next = head;
        head = new_fifo;
    }
finish:
    pthread_mutex_unlock(&fifo_list_mutex);
    return result;
}

int socket_connect_fifo(const char* socket_name, struct fifo_descriptor_s *fifo_descriptor)
{
    int result = 0;
    struct socket_list_s *found_fifo;

    pthread_mutex_lock(&fifo_list_mutex);

    found_fifo = fifo_find(socket_name);
    
    if(NULL == found_fifo) {
        last_error = MSG_SOCKET_FIFO_NOT_FOUND;
        result = -1;
        goto finish;
    }

    fifo_descriptor->fifo_buffer_in = found_fifo->fifo.fifo_buffer_out;
    fifo_descriptor->fifo_buffer_out = found_fifo->fifo.fifo_buffer_in;
    fifo_descriptor->event_fd_in = found_fifo->fifo.event_fd_out;
    fifo_descriptor->event_fd_out = found_fifo->fifo.event_fd_in;

finish:
    pthread_mutex_unlock(&fifo_list_mutex);
    return result;
}

int socket_write_fifo_ptr(struct fifo_descriptor_s *fifo_descriptor, void* data, size_t size)
{
    uint64_t u = 1;
    if( 0 == fifo_push(fifo_descriptor->fifo_buffer_out, data, size) ) {
        write(fifo_descriptor->event_fd_out, &u, sizeof(u));
        return 0;
    }

    return -1;
}

ssize_t socket_read_fifo_ptr(struct fifo_descriptor_s *fifo_descriptor, void **data) 
{
    uint64_t u;
    ssize_t result;
    if( read(fifo_descriptor->event_fd_in, &u, sizeof(u)) != sizeof(u)) {
        return -1;
    }

    result = fifo_pull(fifo_descriptor->fifo_buffer_in, data);
    return result;
}
