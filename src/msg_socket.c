#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "socket_common.h"
#include "socket_fifo.h"
#include "socket_unix.h"
#include "socket_udp.h"
#include "socket_tcp.h"
#include "msg_socket.h"

__thread int last_error;

typedef enum {
    SOCK_TYPE_UDP,
    SOCK_TYPE_UNIX,
    SOCK_TYPE_FIFO,
    SOCK_TYPE_TCP,
    SOCK_TYPE_MAX
} socket_type;

struct msg_socket_s {
    socket_type type;
    char socket_name[256];
    union {
        int fd; // For UNIX and UDP sockets
        struct fifo_descriptor_s fifo_descriptor;
        struct tcp_descriptor_s tcp_descriptor;
    } socket_descriptor;
};

static const char* err_str[];

static int parse_socket_name(struct msg_socket_s *msg_sock, const char *name)
{
    socket_type sock_type;
    size_t prefix_len;
    size_t name_len = strlen(name);

    if( (name_len > 7) && (0 == strncmp(name, "unix://", 7)) ) {
        prefix_len = 7;
        sock_type = SOCK_TYPE_UNIX;
    } else if( (name_len > 7) && (0 == strncmp(name, "fifo://", 7)) ) {
        prefix_len = 7;
        sock_type = SOCK_TYPE_FIFO;
    } else if( (name_len > 6) && (0 == strncmp(name, "udp://", 6)) ) {
        prefix_len = 6;
        sock_type = SOCK_TYPE_UDP;
    } else if( (name_len > 6) && (0 == strncmp(name, "tcp://", 6)) ) {
        prefix_len = 6;
        sock_type =SOCK_TYPE_TCP;
    } else {
        last_error = MSG_SOCKET_UNKNOWN_SOCKET_TYPE;
        return -1;
    }

    msg_sock->type = sock_type;
    strncpy(msg_sock->socket_name, name + prefix_len, sizeof(msg_sock->socket_name));
    printf("Socket type: %d, Socket name: %s\n", msg_sock->type, msg_sock->socket_name);
    return 0;
}

int msg_socket_create(struct msg_socket_s **msg_sock)
{
    int result = 0;
    last_error = MSG_SOCKET_OK;
    
    *msg_sock = (struct msg_socket_s*)calloc(1, sizeof(struct msg_socket_s));
    if( NULL == *msg_sock ) {
        last_error = MSG_SOCKET_OUT_OF_MEMORY;
        result = -1;
    }
    (*msg_sock)->socket_descriptor.fd = -1;

    return result;
}

int msg_socket_bind(struct msg_socket_s *msg_sock, const char* address)
{
    int result = 0;
    struct sockaddr sock_addr;
    last_error = MSG_SOCKET_OK;

    result = parse_socket_name(msg_sock, address);
    if( -1 == result ) {
        goto finish;
    }

    switch(msg_sock->type) {
        case SOCK_TYPE_UNIX:
            result = socket_bind_unix(msg_sock->socket_name, &msg_sock->socket_descriptor.fd);
            break;
        case SOCK_TYPE_UDP:
            result = socket_bind_udp(msg_sock->socket_name, &msg_sock->socket_descriptor.fd);
            break;
        case SOCK_TYPE_TCP:
            result = socket_bind_tcp(msg_sock->socket_name, &msg_sock->socket_descriptor.tcp_descriptor);            
            break;
        case SOCK_TYPE_FIFO:
            result = socket_bind_fifo(msg_sock->socket_name, &msg_sock->socket_descriptor.fifo_descriptor);
            break;
    }

finish:
    return result;
}

int msg_socket_connect(struct msg_socket_s *msg_sock, const char* address) 
{
    int result = 0;
    struct sockaddr sock_addr;
    last_error = MSG_SOCKET_OK;

    result = parse_socket_name(msg_sock, address);
    if( -1 == result ) {
        goto finish;
    }

    switch(msg_sock->type) {
        case SOCK_TYPE_UNIX:
            result = socket_connect_unix(msg_sock->socket_name, &msg_sock->socket_descriptor.fd);
            break;
        case SOCK_TYPE_UDP:
            result = socket_connect_udp(msg_sock->socket_name, &msg_sock->socket_descriptor.fd);
            break;
        case SOCK_TYPE_TCP:
            result = socket_connect_tcp(msg_sock->socket_name, &msg_sock->socket_descriptor.tcp_descriptor);            
            break;
        case SOCK_TYPE_FIFO:
            result = socket_connect_fifo(msg_sock->socket_name, &msg_sock->socket_descriptor.fifo_descriptor);
            break;
    }

finish:
    return result;
}

ssize_t msg_socket_send(struct msg_socket_s *msg_sock, void *buffer, size_t buffer_size)
{
    ssize_t result;
    last_error = MSG_SOCKET_OK;
    void *fifo_temprorary_buffer;

    switch(msg_sock->type) {
        case SOCK_TYPE_UNIX:
        case SOCK_TYPE_UDP:
            result = send(msg_sock->socket_descriptor.fd, buffer, buffer_size, 0);
            break;
        case SOCK_TYPE_TCP:
            result = socket_tcp_write_datagram(&msg_sock->socket_descriptor.tcp_descriptor, buffer, buffer_size);
            break;
        case SOCK_TYPE_FIFO:
            fifo_temprorary_buffer = malloc(buffer_size);
            if( NULL == fifo_temprorary_buffer ) {
                last_error = MSG_SOCKET_OUT_OF_MEMORY;
                return -1;
            }
            memcpy(fifo_temprorary_buffer, buffer, buffer_size);
            result = socket_write_fifo_ptr(&msg_sock->socket_descriptor.fifo_descriptor, fifo_temprorary_buffer, buffer_size);
            if( -1 == result ) {
                free(fifo_temprorary_buffer);
                result = -1;
            }
            
            break;
        default:
            last_error = MSG_SOCKET_UNDEFINED_SOCKET_TYPE;
            return -1;
    }

    if( -1 == result ) {
        last_error = MSG_SOCKET_SEND_ERROR;
    }

    return result;
}

ssize_t msg_socket_send_unsafe(struct msg_socket_s *msg_sock, void *buffer, size_t buffer_size)
{
    switch(msg_sock->type) {
        case SOCK_TYPE_FIFO:
            return socket_write_fifo_ptr(&msg_sock->socket_descriptor.fifo_descriptor, buffer, buffer_size);
        default:
            last_error = MSG_SOCKET_TYPE_NOT_SUPPORTED;
            return -1;
    }
}

ssize_t msg_socket_receive(struct msg_socket_s *msg_sock, void *buffer, size_t buffer_size)
{
    ssize_t result = 0, packet_size;
    last_error = MSG_SOCKET_OK;
    void *temprorary_buffer;

    switch(msg_sock->type) {
        case SOCK_TYPE_UNIX:
        case SOCK_TYPE_UDP:
            result = recv(msg_sock->socket_descriptor.fd, buffer, buffer_size, 0);
            break;
        case SOCK_TYPE_TCP:
            break; 
            packet_size = socket_tcp_read_fifo_ptr(&msg_sock->socket_descriptor.tcp_descriptor, &temprorary_buffer);
            if( (0 < packet_size) && (packet_size <= buffer_size) ) {
                memcpy(buffer, temprorary_buffer, packet_size);
                result = packet_size;
                free(temprorary_buffer);
                break;
            }
            result = -1;
            break;
        case SOCK_TYPE_FIFO:
            packet_size = socket_read_fifo_ptr(&msg_sock->socket_descriptor.fifo_descriptor, &temprorary_buffer);
            if( (0 < packet_size) && (packet_size <= buffer_size) ) {
                memcpy(buffer, temprorary_buffer, packet_size);
                result = packet_size;
                free(temprorary_buffer);
                break;
            }
            result = -1;
            break;
        default:
            last_error = MSG_SOCKET_UNDEFINED_SOCKET_TYPE;
            return -1;
    }

    if( -1 == result ) {
        last_error = MSG_SOCKET_RECEIVE_ERROR;
    }

    return result;
}

ssize_t msg_socket_receive_unsafe(struct msg_socket_s *msg_sock, void **data) 
{
    switch(msg_sock->type) {
        case SOCK_TYPE_FIFO:
            return socket_read_fifo_ptr(&msg_sock->socket_descriptor.fifo_descriptor, data);
        default:
            last_error = MSG_SOCKET_TYPE_NOT_SUPPORTED;
            return -1;
    }
}

int msg_socket_get_poll_fd(struct msg_socket_s *msg_sock)
{
    if( (SOCK_TYPE_UDP == msg_sock->type) || (SOCK_TYPE_UNIX == msg_sock->type) ) {
        return msg_sock->socket_descriptor.fd;
    }
    switch(msg_sock->type) {
        case SOCK_TYPE_UDP:
        case SOCK_TYPE_UNIX:
            return msg_sock->socket_descriptor.fd;
        case SOCK_TYPE_FIFO:
            return msg_sock->socket_descriptor.fifo_descriptor.event_fd_in;
        default:
            last_error = MSG_SOCKET_UNDEFINED_SOCKET_TYPE;
            return -1;
    }
}


int msg_socket_get_last_error() 
{
    return last_error;
}

const char* msg_socket_error_string(int error)
{
    return err_str[((error <= MSG_SOCKET_ERR_MAX) && (error >= 0)) ? error : MSG_SOCKET_ERR_MAX];
}

static const char* err_str[] = {
    "No Error",
    "Unknown socket type",
    "Unable to create socket",
    "Unable to bind socket",
    "Unable to connect socket",
    "Out of memory",
    "Unable to send",
    "Unable to receive",
    "Invalid IP address", 
    "Socket type is undefined",
    "Socket type not supported",
    "FIFO Buffer already exists",
    "FIFO Buffer not found",
    "Invalid error number"
};
