#ifndef SOCKET_COMMON_H
#define SOCKET_COMMON_H

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/un.h>

#define MIN(a,b) \
    ({ __typeof__(a) _a = (a); \
       __typeof__(b) _b = (b); \
       _a < _b ? _a : _b; })

typedef enum {
    SOCKET_BIND,
    SOCKET_CONNECT
} socket_operation_e;

extern __thread int last_error;

int parse_socket_name_to_ip(const char* socket_name, char* ip_addr, size_t ip_addr_len, uint16_t *port);
int socket_bind_connect_ip(const char* socket_name, int socket_type, socket_operation_e op);

#endif
