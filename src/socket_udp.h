#ifndef SOCKET_UDP_H
#define SOCKET_UDP_H

int socket_bind_udp(const char *socket_name, int *fd);
int socket_connect_udp(const char *socket_name, int *fd);

#endif
