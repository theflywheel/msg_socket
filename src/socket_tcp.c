#include <pthread.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>

#include "socket_common.h"
#include "socket_defs.h"
#include "socket_tcp.h"

#define MAX_EVENTS 10
#define MAX_FIFO_LEN 100

static pthread_once_t tcp_thread_initializer = PTHREAD_ONCE_INIT;
static pthread_t tcp_runner_thread;
static int epoll_fd;

static void* tcp_runner_func(void* arg) 
{
    struct epoll_event epoll_events[MAX_EVENTS], epoll_event;
    struct tcp_descriptor_s *tcp_desc;
    int num_events, i, bytes_read;
    uint64_t event_fd_data;

    for(;;) {
        num_events = epoll_wait(epoll_fd, epoll_events, MAX_EVENTS, -1);
        for(i = 0; i < num_events; ++i) {
            tcp_desc = (struct tcp_descriptor_s*)epoll_events[i].data.ptr;
            if(tcp_desc->listening_fd == tcp_desc->epoll_fd) {
                if(-1 != tcp_desc->connection_fd) {
                    if( -1 != (tcp_desc->connection_fd = accept(tcp_desc->listening_fd, NULL, NULL)) ) {
                        tcp_desc->epoll_fd = tcp_desc->connection_fd;
                        epoll_event.events = EPOLLIN;
                        epoll_event.data.ptr = (void*)tcp_desc;
                        epoll_ctl(epoll_fd, EPOLL_CTL_ADD, tcp_desc->connection_fd, &epoll_event);
                    }
                }
            } else if(tcp_desc->connection_fd == tcp_desc->epoll_fd) {
                switch(tcp_desc->state) {
                    case WAITING_SIZE:
                        tcp_desc->bytes_left = sizeof(tcp_desc->current_packet_size);
                        tcp_desc->state = READING_SIZE;
                    case READING_SIZE:
                        bytes_read = read(tcp_desc->connection_fd, 
                            (void*)&tcp_desc->current_packet_size + sizeof(tcp_desc->current_packet_size) - tcp_desc->bytes_left, 
                            tcp_desc->bytes_left);
                        if(bytes_read > 0) {
                            tcp_desc->bytes_left -= bytes_read;
                            if(0 == tcp_desc->bytes_left) {
                                tcp_desc->current_packet_size = ntohl(tcp_desc->current_packet_size);
                                tcp_desc->current_packet_data = malloc(tcp_desc->current_packet_size);

                                tcp_desc->bytes_left = tcp_desc->current_packet_size;
                                tcp_desc->state = READING_DATA; 
                            } else {
                                // It's so fucking unlikely if 4 bytes will be read in multiple read operations, 
                                // but we have to take into account each piece of shit, to avoid suprises
                                break;
                            }
                        } else {
                            //Some shit happened
                            break;
                        }
                    case READING_DATA:
                        // Finally! We've got this fucking packet size! 
                        bytes_read = read(tcp_desc->connection_fd, 
                            tcp_desc->current_packet_data + tcp_desc->current_packet_size - tcp_desc->bytes_left,
                            tcp_desc->bytes_left);
                        if(bytes_read > 0) {
                            tcp_desc->bytes_left -= bytes_read;
                            if(0 == tcp_desc->bytes_left) {
                                // Yes, we did it! We've got the whole packet
                                if( -1 == fifo_push(tcp_desc->packet_buffer, 
                                                        tcp_desc->current_packet_data, 
                                                        tcp_desc->current_packet_size) ) {
                                    // Some shit happened. We should handle it, but not now,
                                    // because it's fucking complicated 
                                    // If the buffer is full, we should switch from level to edge
                                    // triggering and stop reading the socket until the fifo have enouch space
                                }
                                event_fd_data = 1;
                                write(tcp_desc->event_fd, &event_fd_data, sizeof(uint64_t));
                                tcp_desc->state = WAITING_SIZE;
                            }
                        }
                        break;
                }
            }
        }
    }
}

static void start_tcp_thread() 
{
    epoll_fd = epoll_create1(0);
    if( -1 != epoll_fd ) {
        pthread_create(&tcp_runner_thread, NULL, &tcp_runner_func, NULL);
    }
}

int socket_bind_tcp(const char* socket_name, struct tcp_descriptor_s *tcp_desc)
{
    struct epoll_event ep_event;
    int result = -1;

    pthread_once(&tcp_thread_initializer, start_tcp_thread);

    tcp_desc->state = WAITING_SIZE;
    tcp_desc->listening_fd = socket_bind_connect_ip(socket_name, SOCK_STREAM, SOCKET_BIND);
    if( -1 == listen(tcp_desc->listening_fd, 1) ) {
        goto finish_close;
    }
    tcp_desc->connection_fd = -1;
    tcp_desc->epoll_fd = tcp_desc->listening_fd;
    if(-1 == (tcp_desc->event_fd = eventfd(0, EFD_SEMAPHORE))) {
        goto finish_close;
    }

    fifo_create(&tcp_desc->packet_buffer, MAX_FIFO_LEN);

    ep_event.events = EPOLLIN;
    ep_event.data.ptr = (void*)tcp_desc;
    if( -1 == epoll_ctl(epoll_fd, EPOLL_CTL_ADD, tcp_desc->listening_fd, &ep_event) ) {
        goto finish_remove_fifo;
    }

    result = 0;
    goto finish;

finish_remove_fifo:
    fifo_destroy(&tcp_desc->packet_buffer); 
    close(tcp_desc->event_fd);
finish_close:
    close(tcp_desc->listening_fd);
finish:
    return result;
}

int socket_connect_tcp(const char* socket_name, struct tcp_descriptor_s *tcp_desc)
{
    struct epoll_event ep_event;
    int result = -1;

    pthread_once(&tcp_thread_initializer, start_tcp_thread);

    tcp_desc->state = WAITING_SIZE;
    tcp_desc->listening_fd = -1;
    tcp_desc->connection_fd = socket_bind_connect_ip(socket_name, SOCK_STREAM, SOCKET_CONNECT);
    if(-1 == tcp_desc->connection_fd) {
        goto finish_close;
    }

    tcp_desc->epoll_fd = tcp_desc->connection_fd;
    if(-1 == (tcp_desc->event_fd = eventfd(0, EFD_SEMAPHORE))) {
        goto finish_close;
    }

    fifo_create(&tcp_desc->packet_buffer, MAX_FIFO_LEN);

    ep_event.events = EPOLLIN;
    ep_event.data.ptr = (void*)tcp_desc;
    if(-1 == epoll_ctl(epoll_fd, EPOLL_CTL_ADD, tcp_desc->connection_fd, &ep_event)) {
        goto finish_remove_fifo;
    }
    
    result = 0;
    goto finish;

finish_remove_fifo:
    fifo_destroy(&tcp_desc->packet_buffer);
    close(tcp_desc->event_fd);
finish_close:
    close(tcp_desc->connection_fd);
finish:
    return result;

}

ssize_t socket_tcp_write_datagram(struct tcp_descriptor_s *tcp_desc, void *data, uint32_t size) 
{
    struct iovec io_vector[2];
    
    io_vector[1].iov_base = data;
    io_vector[1].iov_len = size;
    size = htonl(size);
    io_vector[0].iov_base = (void*)&size;
    io_vector[0].iov_len = sizeof(size);
    return writev(tcp_desc->connection_fd, io_vector, 2);
}

ssize_t socket_tcp_read_datagram_ptr(struct tcp_descriptor_s *tcp_desc, void** data_ptr)
{
    uint64_t u;

    if(read(tcp_desc->event_fd, &u, sizeof(uint64_t)) != sizeof(uint64_t)) {
        return -1;
    }
    return fifo_pull(tcp_desc->packet_buffer, data_ptr);
}
