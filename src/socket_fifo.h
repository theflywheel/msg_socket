#ifndef SOCKET_FIFO_H
#define SOCKET_FIFO_H

#include <fifo.h>

struct fifo_descriptor_s {
    fifo_t *fifo_buffer_in;
    fifo_t *fifo_buffer_out;
    int event_fd_in;
    int event_fd_out;
};

int socket_bind_fifo(const char* socket_name, struct fifo_descriptor_s *fifo_descriptor);
int socket_connect_fifo(const char* socket_name, struct fifo_descriptor_s *fifo_descriptor);
int socket_write_fifo_ptr(struct fifo_descriptor_s *fifo_descriptor, void* data, size_t size);
ssize_t socket_read_fifo_ptr(struct fifo_descriptor_s *fifo_descriptor, void **data);

#endif
