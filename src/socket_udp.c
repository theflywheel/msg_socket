#include <stdio.h>

#include "socket_common.h"
#include "socket_udp.h"
#include "socket_defs.h"

int socket_bind_udp(const char* socket_name, int *fd)
{
    char ip_addr_str[INET6_ADDRSTRLEN + 1];
    char ip_addr[16]; //ipv6 addres length, since buffer will be used for both ipv4 and ipv6
    uint16_t port;

    if( -1 == parse_socket_name_to_ip(socket_name, ip_addr_str, sizeof(ip_addr_str), &port) ) {
        return -1;
    }

    if( inet_pton(AF_INET, ip_addr_str, ip_addr) ) {
        struct sockaddr_in s_a;
        *fd = socket(AF_INET, SOCK_DGRAM, 0);
        if( -1 == *fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in));
        s_a.sin_family = AF_INET;
        s_a.sin_addr.s_addr = *((unsigned long*)ip_addr);
        s_a.sin_port = htons(port);
        if( -1 == bind(*fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
            last_error = MSG_SOCKET_BIND_ERROR;
            return -1;
        }
    } else if( inet_pton(AF_INET6, ip_addr_str, ip_addr) ) {
        struct sockaddr_in6 s_a;
        *fd = socket(AF_INET6, SOCK_DGRAM, 0);
        if( -1 == *fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in6));
        s_a.sin6_family = AF_INET6;
        s_a.sin6_flowinfo = 0;
        s_a.sin6_scope_id = 0;
        s_a.sin6_port = htons(port);
        memcpy(s_a.sin6_addr.s6_addr, ip_addr, sizeof(s_a.sin6_addr.s6_addr));
        if( -1 == bind(*fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
            last_error = MSG_SOCKET_BIND_ERROR;
            return -1;
        }
    } else {
        last_error = MSG_SOCKET_INVALID_IP_ADDRESS;
        return -1;
    }

    return 0;
}

int socket_connect_udp(const char* socket_name, int *fd) 
{
    char ip_addr_str[INET6_ADDRSTRLEN + 1];
    char ip_addr[16];
    uint16_t port;

    if( -1 == parse_socket_name_to_ip(socket_name, ip_addr_str, sizeof(ip_addr_str), &port) ) {
        return -1;
    }

    if( inet_pton(AF_INET, ip_addr_str, ip_addr) ) {
        struct sockaddr_in s_a;
        *fd = socket(AF_INET, SOCK_DGRAM, 0);
        if( -1 == *fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in));
        s_a.sin_family = AF_INET;
        s_a.sin_addr.s_addr = *((unsigned long*)ip_addr);
        s_a.sin_port = htons(port);
        if( -1 == connect(*fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
            last_error = MSG_SOCKET_CONNECT_ERROR;
            return -1;
        }
    } else if( inet_pton(AF_INET6, ip_addr_str, ip_addr) ) {
        struct sockaddr_in6 s_a;
        *fd = socket(AF_INET6, SOCK_DGRAM, 0);
        if( -1 == *fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in6));
        s_a.sin6_family = AF_INET6;
        s_a.sin6_flowinfo = 0;
        s_a.sin6_scope_id = 0;
        s_a.sin6_port = htons(port);
        memcpy(s_a.sin6_addr.s6_addr, ip_addr, sizeof(s_a.sin6_addr.s6_addr));
        if( -1 == connect(*fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
            last_error = MSG_SOCKET_CONNECT_ERROR;
            return -1;
        }
    } else {
        last_error = MSG_SOCKET_INVALID_IP_ADDRESS;
        return -1;
    }

    return 0;
}

