#ifndef FIFO_ARRAY_H
#define FIFO_ARRAY_H 

#include <stdint.h>
#include <stdlib.h>

typedef union {
  uint64_t qw[2]; //!< The qword access to the union
  struct {
    volatile uint32_t head_index; //!< Head index
    volatile uint32_t tail_index; //!< Tail index
    volatile uint32_t size; //!< The current size of the FIFO
    volatile uint32_t counter; //!< ABA problem prevention counter
  } __attribute__ ((__aligned__( 16 ), __packed__));
#ifdef __x86_64__
  __int128_t atomic;
#else
  int64_t atomic;
#endif
} index_u;

struct data_chunk_s {
    union {
        struct {
            void *data;
            uint64_t size;
        };
        __int128_t atomic;
    };
};

//! Structure for holding the lock-free FIFO state and data
struct fifo_s {
  struct data_chunk_s *data;
  index_u *index; //!< Auxiliary FIFO data
  uint32_t max_size; //!< Max FIFO size
};

typedef struct fifo_s fifo_t;

//! Creates a lock-free FIFO structure with ability to grow up to the max_size elements
void fifo_create(fifo_t **fifo, uint32_t max_size); 
//! Destroys a lock-free FIFO structure previously created by fifo_create
void fifo_destroy(fifo_t **fifo); 
//! Pushes an element 'data' onto lock-free FIFO fifo
int fifo_push(fifo_t *fifo, void *data, size_t size);
//! Pulls an element from the lock-free FIFO fifo
ssize_t fifo_pull(fifo_t *fifo, void **data);
//! Returns the FIFO size
static inline int fifo_size(fifo_t *fifo) { return fifo->index->size; }

#endif /* FIFO_ARRAY_H */
