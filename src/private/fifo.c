#include "fifo.h"
#include <malloc.h>
#include <string.h>

/** Creates a lock-free FIFO structure with ability to grow up to the max_size elements
 * @param fifo the pointer to the variable that will hold the fifo
 * @param max_size the max amount of elements that the fifo will be able to accommodate
 * @see fifo_destroy
 */
void fifo_create(fifo_t **fifo, uint32_t max_size) {
  // Allocating the structure
  *fifo = (fifo_t*)malloc(sizeof(fifo_t));
  if(*fifo) {
    // Allocating and initializing data array
    (*fifo)->data = malloc(sizeof(struct data_chunk_s) * max_size);
    memset((*fifo)->data, 0, sizeof(struct data_chunk_s) * max_size);
    // Allocating and initializing the index, that should be aligned on 16 bit boundary
    (*fifo)->index = (index_u*)memalign(16, sizeof(index_u));
    memset((*fifo)->index, 0, sizeof(index_u));
    // Setting max size (and wrap boundary) of the FIFO/data array
    (*fifo)->max_size = max_size;
    __sync_synchronize();
  }
}

/** Destroys a lock-free FIFO structure previously created by fifo_create
 * @param fifo the pointer to the variable that holds the pointer to the fifo
 * @see fifo_create
 */
void fifo_destroy(fifo_t **fifo) {
  // Freeing data
  free((*fifo)->data);
  // Freeing index
  free((*fifo)->index);
  // Freeing the structure
  free(*fifo);
  // Cleaning up the pointer
  *fifo = NULL;
}

/** Pushes an element 'data' onto lock-free FIFO
 * @param fifo the pointer to the fifo
 * @param data the data to push
 * @see fifo_pull
 */
int fifo_push(fifo_t *fifo, void *data, size_t size) {
  index_u index, new_index;
  struct data_chunk_s data_chunk = { data, size };

try_push:
  index.atomic = fifo->index->atomic; 
  // Check if the size is not exceeded
  if(index.size == fifo->max_size) return -1;
  // Initializing new index
  new_index = index;
  new_index.tail_index = ((new_index.tail_index + 1) % fifo->max_size);
  new_index.size++;
  new_index.counter++;
  // Trying to swap indices atomically
  if(!__sync_bool_compare_and_swap(&fifo->index->atomic, index.atomic, new_index.atomic)) {
    usleep(2);
    goto try_push;
  }

  // If succeeded, wait for element to free in case we're writing over the data being pulled and set the element
  while(!__sync_bool_compare_and_swap(&((struct data_chunk_s*)fifo->data)[index.tail_index].atomic, 0, data_chunk.atomic)) {
    usleep(2);
  }; // wait

  return 0;
}

/** Pulls an element from the lock-free FIFO
 * @param fifo the pointer to the fifo
 * @return the pointer to the pulled data if fifo was not empty, NULL otherwise
 * @see fifo_pull
 */
ssize_t fifo_pull(fifo_t *fifo, void **data) {
  index_u index, new_index;
  struct data_chunk_s data_chunk;

  *data = NULL; 

try_pop:
  index = *(fifo->index);
  // Check if there is something in the FIFO
  if(index.size == 0) return -1;
  // Initializing new index
  new_index = index;
  new_index.head_index = ((new_index.head_index + 1) % fifo->max_size);
  new_index.size--;
  new_index.counter++;
  // Trying to swap indices atomically
  if(!__sync_bool_compare_and_swap(&fifo->index->atomic, index.atomic, new_index.atomic)) {
    usleep(1);
    goto try_pop;
  }

  // If succeeded, get the element
get_val:
  data_chunk = ((struct data_chunk_s*)fifo->data)[index.head_index];
    // Wait until the data is set in case we're reading over the element just being pushed
  if(!data_chunk.data) {
    usleep(1);
    goto get_val;
  }

  // "Free" the element (mark it as read)
  ((struct data_chunk_s*)fifo->data)[index.head_index].data = NULL;
  ((struct data_chunk_s*)fifo->data)[index.head_index].size = 0;
  __sync_synchronize();

  *data = data_chunk.data;
  return data_chunk.size;
}
