#include <stdlib.h>

#include "socket_common.h"
#include "socket_defs.h"

int parse_socket_name_to_ip(const char* socket_name, char* ip_addr, size_t ip_addr_len, uint16_t *port) 
{
    char *port_str = strrchr(socket_name, ':');

    if( NULL == port_str) {
        last_error = MSG_SOCKET_INVALID_IP_ADDRESS;
        return -1;
    }

    memset(ip_addr, 0, ip_addr_len);
    strncpy(ip_addr, socket_name, MIN(ip_addr_len - 1, port_str - socket_name));

    port_str++;
    *port = (uint16_t)atoi(port_str);

    return 0;
}

int socket_bind_connect_ip(const char* socket_name, int socket_type, socket_operation_e op)
{
    char ip_addr_str[INET6_ADDRSTRLEN + 1];
    char ip_addr[16]; //ipv6 addres length, since buffer will be used for both ipv4 and ipv6
    int fd;
    uint16_t port;

    if( -1 == parse_socket_name_to_ip(socket_name, ip_addr_str, sizeof(ip_addr_str), &port) ) {
        return -1;
    }

    if( inet_pton(AF_INET, ip_addr_str, ip_addr) ) {
        struct sockaddr_in s_a;
        fd = socket(AF_INET, socket_type, 0);
        if( -1 == fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in));
        s_a.sin_family = AF_INET;
        s_a.sin_addr.s_addr = *((unsigned long*)ip_addr);
        s_a.sin_port = htons(port);

        switch(op) {
            case SOCKET_BIND:
                if( -1 == bind(fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
                    close(fd);
                    last_error = MSG_SOCKET_BIND_ERROR;
                    return -1;
                }
                break;
           case SOCKET_CONNECT:
                if( -1 == connect(fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
                    close(fd);
                    last_error = MSG_SOCKET_CONNECT_ERROR;
                }
        }
    } else if( inet_pton(AF_INET6, ip_addr_str, ip_addr) ) {
        struct sockaddr_in6 s_a;
        fd = socket(AF_INET6, socket_type, 0);
        if( -1 == fd ) {
            last_error = MSG_SOCKET_SOCKET_ERROR;
            return -1;
        }
        memset((void*)&s_a, 0, sizeof(struct sockaddr_in6));
        s_a.sin6_family = AF_INET6;
        s_a.sin6_flowinfo = 0;
        s_a.sin6_scope_id = 0;
        s_a.sin6_port = htons(port);
        memcpy(s_a.sin6_addr.s6_addr, ip_addr, sizeof(s_a.sin6_addr.s6_addr));
        switch(op) {
            case SOCKET_BIND:
                if( -1 == bind(fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
                    close(fd);
                    last_error = MSG_SOCKET_BIND_ERROR;
                    return -1;
                }
                break;
           case SOCKET_CONNECT:
                if( -1 == connect(fd, (struct sockaddr*)&s_a, sizeof(s_a)) ) {
                    close(fd);
                    last_error = MSG_SOCKET_CONNECT_ERROR;
                }
        }
    } else {
        last_error = MSG_SOCKET_INVALID_IP_ADDRESS;
        return -1;
    }

    return fd;
}
