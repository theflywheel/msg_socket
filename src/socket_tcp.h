#ifndef SOCKET_TCP_H
#define SOCKET_TCP_H

#include <fifo.h>

struct tcp_descriptor_s {
    int epoll_fd;
    int listening_fd;
    int connection_fd;
    int event_fd;
    fifo_t *packet_buffer;
    enum { WAITING_SIZE, READING_SIZE, READING_DATA } state;
    size_t bytes_left;
    uint32_t current_packet_size;
    void* current_packet_data;
};

int socket_bind_tcp(const char* socket_name, struct tcp_descriptor_s *tcp_desc);
int socket_connect_tcp(const char* socket_name, struct tcp_descriptor_s *tcp_desc);
ssize_t socket_tcp_write_datagram(struct tcp_descriptor_s *tcp_desc, void *data, uint32_t size);
ssize_t socket_tcp_read_datagram_ptr(struct tcp_descriptor_s *tcp_desc, void** data_ptr);

#endif
