/**
 * @file
 * @brief The msg_socket library
 *
 * This file contains functions that can be used for message transfer
 * @author Alexey Obergan
 * @date 10 May 2017
 */
#ifndef MSG_SOCKET_H
#define MSG_SOCKET_H

#include <sys/types.h>
#include "socket_defs.h"

struct msg_socket_s;

typedef struct msg_socket_s msg_socket_t;

/** Creates the socket. */
int msg_socket_create(struct msg_socket_s **msg_sock);

/** @brief Bind the socket.
 *
 * This function binds the socket to special address. 
 * @param msg_sock a pointer to msg_socket_t object created with @ref msg_socket_create
 * @param address address to bind to. Following strings can be used:
 *  - unix://\<unix-socket-file-name\>
 *  - udp://\<ipv4 or ipv6 address\>:\<port\>
 *  - fifo://\<fifo buffer name\>
 *  @return 0 on success, -1 on failure
 */
int msg_socket_bind(struct msg_socket_s *msg_sock, const char* address);

/** @brief Connect to the socket.
 *
 * This function connects to the socket
 * @param [in] msg_sock a pointer to msg_socket_t object created with @ref msg_socket_create
 * @param [in] address address to bind to. The same as at @ref msg_socket_bind
 * @return 0 on success -1 on failure
 */
int msg_socket_connect(struct msg_socket_s *msg_sock, const char* address);

/** Writes data to socket
 * @param msg_sock Socket to write to
 * @param buffer Data to send
 * @param buffer_size The size of data chunk
 * @return The number of sent bytes on success or -1 on failure
 */
ssize_t msg_socket_send(struct msg_socket_s *msg_sock, void *buffer, size_t buffer_size);

/** The same as @ref msg_socket_send with only exception. 
 * This function doesn't copy data blocks. The user should take care about allocating and freing 
 * the data buffer
 * @param msg_sock Socket to write to
 * @param buffer Data to send
 * @param buffer_size The size of data chunk
 * @return The number of sent bytes on success or -1 on failure
 * @note This function works only with socket type FIFO. When used with other sockets, the function
 * returns -1 and sets last_error(see @ref msg_socket_get_last_error) to MSG_SOCKET_TYPE_NOT_SUPPORTED
 */
ssize_t msg_socket_send_unsafe(struct msg_socket_s *msg_sock, void *buffer, size_t size);

/** Reads data from socket
 * @param msg_sock Socket to read from 
 * @param buffer buffer to read
 * @param buffer_size The size of buffer
 * @return The number of received bytes on success or -1 on failure
 */
ssize_t msg_socket_receive(struct msg_socket_s *msg_sock, void *buffer, size_t buffer_size);

/** The same as @ref msg_socket_receve, but reads only pointer saved in socket
 * @param msg_sock Socket to read from 
 * @param buffer buffer to read
 * @return The size of received buffer on success or -1 on failure
 * @note This function works only with sockets of type FIFO. When used with other sockets, 
 * the function returns -1 and set last_error(see @ref msg_socket_get_last_error) to mSG_SOCKET_TYPE_NOT_SUPPORTED
 */
ssize_t msg_socket_receive_unsafe(struct msg_socket_s *msg_sock, void **data);

/** Returns socket descriptor for using in select or epoll
 * @param msg_sock a pointer to msg_socket_t object
 * @return socket descriptor
 *
 * @note This function returns __event descriptor__. Please use it only in _select_ or _epoll_.
 */
int msg_socket_get_poll_fd(struct msg_socket_s *msg_sock);

/** Returns last error message
 * @return the last error number in msg_socket functions
 */
int msg_socket_get_last_error();
const char* msg_socket_error_string(int error);

#endif  
