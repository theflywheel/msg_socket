#include <stdio.h>
#include <sys/un.h>

#include "socket_common.h"
#include "socket_unix.h"
#include "socket_defs.h"

int socket_bind_unix(const char *socket_name, int *fd) 
{
    int result = 0;
    struct sockaddr_un sock_addr;
            
    last_error = MSG_SOCKET_OK;
    *fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if( -1 == *fd) {
        result = -1;
        last_error = MSG_SOCKET_SOCKET_ERROR;
        goto finish;
    }

    sock_addr.sun_family = AF_UNIX;
    strncpy(sock_addr.sun_path, socket_name, sizeof(sock_addr.sun_path));
    result = bind(*fd, (struct sockaddr*)&sock_addr, sizeof(struct sockaddr_un));

    if( -1 == result) {
        last_error = MSG_SOCKET_BIND_ERROR;
    }

finish:
    return result;
}

int socket_connect_unix(const char *socket_name, int *fd)
{
    int result = 0;
    struct sockaddr_un sock_addr;
    last_error = MSG_SOCKET_OK;

    *fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if( -1 == *fd) {
        result = -1;
        last_error = MSG_SOCKET_SOCKET_ERROR;
        goto finish;
    }

    sock_addr.sun_family = AF_UNIX;
    strncpy(sock_addr.sun_path, socket_name, sizeof(sock_addr.sun_path));
    result = connect(*fd, (struct sockaddr*)&sock_addr, sizeof(struct sockaddr_un));

    if( -1 == result) {
        last_error = MSG_SOCKET_CONNECT_ERROR;
    }

finish:
    return result;
}

