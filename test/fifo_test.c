#include <msg_socket.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

static char message[] = "Hello world!!!";
static char answer[] = "Hello too bro!!";
static msg_socket_t *my_server_socket, *my_client_socket;
static pthread_t reader_thread;

void* reader_func(void *arg) 
{
    char *received_data;
    ssize_t bytes_received;
    msg_socket_create(&my_client_socket);
    if( -1 == msg_socket_connect(my_client_socket, "fifo://my_sock") ) {
        printf("Connect error: %s\n", msg_socket_error_string(msg_socket_get_last_error()));
    }
    bytes_received = msg_socket_receive_unsafe(my_client_socket, &received_data);
    if( -1 != bytes_received) {
        printf("Received %ld bytes: %s\n", bytes_received, received_data);
    } else {
        printf("Receive error: %s\n", msg_socket_error_string(msg_socket_get_last_error()));
    }
    msg_socket_send_unsafe(my_client_socket, answer, sizeof(answer));
}

int main()
{
    char *received_data;
    ssize_t bytes_received;
    msg_socket_create(&my_server_socket); 
    if( -1 == msg_socket_bind(my_server_socket, "fifo://my_sock") ) {
        printf("Bind error: %s\n", msg_socket_error_string(msg_socket_get_last_error()));
    }
    pthread_create(&reader_thread, NULL, &reader_func, NULL);
    sleep(1);

    if( -1 != msg_socket_send_unsafe(my_server_socket, message, sizeof(message)) ) {
        printf("Sent succesful\n");
    } else {
        printf("Send error: %s\n", msg_socket_error_string(msg_socket_get_last_error()));
    }

    bytes_received = msg_socket_receive_unsafe(my_server_socket, &received_data);
    if( -1 != bytes_received) {
        printf("Received %ld bytes: %s\n", bytes_received, received_data);
    } else {
        printf("Receive error: %s\n", msg_socket_error_string(msg_socket_get_last_error()));
    }
    pthread_join(reader_thread, NULL);
}
