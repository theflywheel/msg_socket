#include <msg_socket.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

msg_socket_t *my_socket;

int main()
{
    char recv_buffer[1024];
    msg_socket_create(&my_socket);
    if( -1 == msg_socket_bind(my_socket, "udp://::1:12345")) {
        printf("Last error: %s, errno: %s\n", msg_socket_error_string(msg_socket_get_last_error()), strerror(errno));
        return 1;
    }
    while(msg_socket_receive(my_socket, recv_buffer, sizeof(recv_buffer)) != -1) {
        printf("Received buffer: %s\n", recv_buffer);
    }
    return 0;
}
