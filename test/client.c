#include<msg_socket.h>
#include<stdio.h>

msg_socket_t *my_socket;

int main()
{
    char send_string[] = "Hello world!!!";
    msg_socket_create(&my_socket);
    if( -1 == msg_socket_connect(my_socket, "udp://::1:12345")) {
        printf("Last error: %d\n", msg_socket_get_last_error());
        return 1;
    }
    if( -1 == msg_socket_send(my_socket, send_string, sizeof(send_string)) ) {
        printf("Send error: %d\n", msg_socket_get_last_error());
    }
    return 0;
}
